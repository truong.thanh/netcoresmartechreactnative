/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Main from './src/Main';

let identify = '';

function setIdentify(value) {
  identify = value;
}

export { identify, setIdentify }

AppRegistry.registerComponent(appName, () => Main);
