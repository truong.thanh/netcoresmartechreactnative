/**
* This is the Main file
* This file contains the routes of all the pages
**/

// React native and others libraries imports
import React, { Component } from 'react';
import {
  Alert,
  AsyncStorage,
  BackHandler,
  DeviceEventEmitter,
  NativeModules,
  NativeEventEmitter,
  Platform
} from 'react-native';
import { Root } from 'native-base';
import { Scene, Router, Actions } from 'react-native-router-flux';
import { setIdentify } from '../index';


// Our custom files and classes import
import Home from './page/Home';
import Search from './page/Search';
import Cart from './page/Cart';
import WishList from './page/WishList';
import Map from './page/Map';
import Newsletter from './page/Newsletter';
import Contact from './page/Contact';
import Category from './page/Category';
import Product from './page/Product';
import ImageGallery from './page/ImageGallery';
import Login from './page/Login';
import Signup from './page/Signup';
import Checkout from './page/Checkout';

const { DeeplinkManager } = NativeModules;
const deeplinkManagerEmitter = new NativeEventEmitter(DeeplinkManager);

export default class Main extends Component {
  componentWillMount = () => {
    BackHandler.addEventListener('hardwareBackPress', () => Actions.pop());
    this.getCustomData();
  };

  componentDidMount() {
    AsyncStorage.getItem('identify', function (err, value) {
      console.log('refresh storage ', value);
      setIdentify(value);
      Actions.refresh();
    });
    
    if (Platform.OS === 'ios') {
      deeplinkManagerEmitter.addListener('SMTDeeplink', this.handleDeeplink);
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'ios') {
      deeplinkManagerEmitter.removeEventListener('SMTDeeplink', this.handleDeeplink);
    }
  }

  getCustomData = () => {
   // console.log("getCustomData->" + JSON.stringify(this.props.screenProps));
     console.log("getCustomData->" + JSON.stringify(this.props));
  };

  // handle deeplink on iOS
  handleDeeplink(event) {
    console.log(event.deeplink);
    console.log(event.userInfo.customPayload);
    // if (event.deeplink == 'rnsmartech.voolatech://search') {
    //   //your navigation code
    //   Actions.search();
    // }
  }

  render() {
    return(
      <Root>
        <Router
          uriPrefix={'rnsmartech.voolatech'}
        >
          <Scene key="root">
            <Scene initial key="home" component={Home} hideNavBar />
            <Scene key="search" component={Search} modal hideNavBar />
            <Scene key="cart" component={Cart} modal hideNavBar />
            <Scene key="wishlist" component={WishList} modal hideNavBar />
            <Scene key="map" component={Map} modal hideNavBar />
            <Scene key="contact" component={Contact} modal hideNavBar />
            <Scene key="newsletter" component={Newsletter} modal hideNavBar />
            <Scene key="category" component={Category} hideNavBar />
            <Scene key="product" component={Product} hideNavBar />
            <Scene key="imageGallery" component={ImageGallery} modal hideNavBar />
            <Scene key="login" component={Login} hideNavBar />
            <Scene key="signup" component={Signup} hideNavBar />
            <Scene key="checkout" component={Checkout} hideNavBar />
          </Scene>
        </Router>
      </Root>
    );
  }

}
