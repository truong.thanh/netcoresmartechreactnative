/**
* This is the newsletter page
**/

// React native and others libraries imports
import React, { Component } from 'react';
import { Container, View, Icon, Left, Button, Item, Input } from 'native-base';
import { Actions } from 'react-native-router-flux';
import smartech from 'smartech-react-native';

// Our custom files and classes import
import Text from '../component/Text';
import Navbar from '../component/Navbar';
import Colors from '../Colors';

export default class Newsletter extends Component {
  constructor(props) {
      super(props);
      this.state = {
        email: ''
      }
  }

  componentDidMount() {
    smartech.track('App Screen View', {
      screen: 'Newsletter'
    })
  }
  
  render() {
    var left = (
      <Left style={{flex:1}}>
        <Button transparent onPress={() => Actions.pop()}>
          <Icon name="ios-close" size={38} style={{fontSize: 38}} />
        </Button>
      </Left>
    );
    return(
      <Container style={{backgroundColor: '#fdfdfd'}}>
          <Navbar left={left} title="NEWSLETTER" />
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', paddingLeft: 50, paddingRight: 50}}>
              <Item>
                  <Icon active name='ios-mail' />
                  <Input placeholder='Your email address' onChangeText={(text) => this.setState({email: text})}/>
              </Item>
              <View style={{alignItems: 'center'}}>
                <Button onPress={() => this.subscribe()} style={{backgroundColor: Colors.navbarBackgroundColor, marginTop: 20}}>
                  <Text style={{color: '#fdfdfd'}}>Subscribe</Text>
                </Button>
              </View>
            </View>
      </Container>
    );
  }

  subscribe() {
    smartech.setIdentity(this.state.email)
        .then(result => {
        //Your code here
        alert('Subscribe address: ' + this.state.email);
    }).catch(reason => console.log(reason));
  }
}
