//
//  NotificationService.h
//  SmartechNSE
//
//  Created by Truong Thanh on 9/8/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
