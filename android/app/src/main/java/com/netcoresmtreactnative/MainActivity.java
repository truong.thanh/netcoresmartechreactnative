package com.netcoresmtreactnative;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class MainActivity extends ReactActivity {
    String TAG = "MainActivity";
    String customData = "No Data";
    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "NetcoreSmtReactNative";
    }

//    @Override
//    protected ReactActivityDelegate createReactActivityDelegate() {
//        return new ReactActivityDelegate(this, getMainComponentName()) {
//            @Override
//            protected ReactRootView createRootView() {
//                return new RNGestureHandlerEnabledRootView(MainActivity.this);
//            }
//        };
//    }
//    @Override
//    protected ReactActivityDelegate createReactActivityDelegate() {
//        return new ReactActivityDelegate(this, getMainComponentName()) {
//            @Override
//            protected ReactRootView createRootView() {
//                return new RNGestureHandlerEnabledRootView(MainActivity.this);
//            }
//
//            @Override
//            protected Bundle getLaunchOptions() {
//                String customData = "No Data";
//                try {
//                    Bundle bundle = getIntent().getExtras();
//                    if(bundle!=null)
//                         for (String key : bundle.keySet()) {
//                             Log.e(TAG, key+" : "+bundle.get("customPayload").toString());
//                             customData = bundle.get(key).toString();
//                             // Toast.makeText(MainActivity.this,customData,Toast.LENGTH_LONG).show();
//                         }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                Bundle initialProperties = new Bundle();
//                initialProperties.putString("customData", customData);
//
////                WritableMap eventData = Arguments.createMap();
//
//                // Fill in eventData; details not important
////                eventData.putString("customPayload", customData);
//
////                ReactInstanceManager mReactInstanceManager = getReactNativeHost().getReactInstanceManager();
////                ReactApplicationContext context = (ReactApplicationContext) mReactInstanceManager.getCurrentReactContext();
////                mReactInstanceManager.addReactInstanceEventListener(new ReactInstanceManager.ReactInstanceEventListener() {
////                    public void onReactContextInitialized(ReactContext validContext) {
////                        // Use validContext here
////                        validContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
////                                .emit("handleNotificationPayloadAndroid", eventData);
////                    }
////                });
//
//                return initialProperties;
//            }
//        };
//    }
            @Override
            protected ReactActivityDelegate createReactActivityDelegate() {
                return new ReactActivityDelegate(this, getMainComponentName()) {
                    @Override
                    protected Bundle getLaunchOptions() {
                        try {
                            Bundle bundle = getIntent().getExtras();
                            if(bundle!=null)
                                for (String key : bundle.keySet()) {
                                    Log.e(TAG, key+" : "+bundle.get("customPayload").toString());
                                    customData = bundle.get(key).toString();
                                    Toast.makeText(MainActivity.this,customData,Toast.LENGTH_LONG).show();
                                }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Bundle initialProperties = new Bundle();
                        initialProperties.putString("customData", customData);
                        return initialProperties;
                    }
                };
            }

}
