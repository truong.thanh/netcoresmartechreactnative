package com.netcoresmtreactnative;

import android.app.Application;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.google.firebase.iid.FirebaseInstanceId;
import com.smartech.reactnative.SMTSmartechReactNativePackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;

import java.util.Arrays;
import java.util.List;

import in.netcore.smartechfcm.NetcoreSDK;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new SMTSmartechReactNativePackage(),
            new RNGestureHandlerPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    NetcoreSDK.setPushToken((MainApplication)this.getApplicationContext(), FirebaseInstanceId.getInstance().getToken());
    NetcoreSDK.register((MainApplication)this.getApplicationContext(), "e7df984fa5892911af1cd0ffae51ebd0");

    NotificationCompat.Builder notification = new NotificationCompat.Builder(this);
    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      notification.setSmallIcon(R.drawable.ic_notification);
      notification.setColor(getResources().getColor(R.color.notification_color));
    } else {
      notification.setSmallIcon(R.drawable.ic_notification);
        notification.setColor(getResources().getColor(R.color.notification_color));
    }
  }
}
